# oneleif Active Projects
* [NoteMapV2](https://github.com/zmeriksen/NoteMapV2)
* [kyy](https://github.com/parshav/kyy)
* [ZombieMixer](https://github.com/ambid17/ZombieMixer)
* [SwiftObject](https://gitlab.com/_leif/SwiftObject)

## CocoaPods
* [SwiftObject](http://cocoapods.org/pods/SwiftObject)

## oneleif Discord Link
[Discord](https://discord.gg/wkekGe3)
